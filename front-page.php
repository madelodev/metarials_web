<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Metarials
 */

get_header();
?>
    <div class="preloader" style="display: none;"><span class="preloader-js"></span>
        <div class="blur_top"></div>
        <div class="blur_bottom"></div>
        <div class="preloader_text" style="display: none;">METARIALS</div>
    </div>
    <script>
        //after window is loaded completely
        setTimeout(function () {
            jQuery('.preloader_text').fadeOut(300);
        }, 4000);
        setTimeout(function () {
            jQuery('.preloader').fadeOut(500);
        }, 4600);
    </script>
    <div class="subpage">
        <?php

        $hero_title = get_field('hero_title');
        $hero_subtitle = get_field('hero_subtitle');
        $hero_button = get_field('hero_button');
        $video_close = get_field('video_close');
        $video_zoom_out = get_field('video_zoom_out');
        $video_walk = get_field('video_walk');
        $video_close_mp4 = get_field('video_close_mp4');
        $video_zoom_out_mp4 = get_field('video_zoom_out_mp4');
        $video_walk_mp4 = get_field('video_walk_mp4');

        // var_dump($video_close);
        ?>
        <div class="homepage-hero mobile_overflow">
            <div class="container">
                <h1 class="homepage-hero-title big-title">
                    <?php echo $hero_title ?>
                </h1>
                <div class="homepage-hero-subtitle">
                    <?php echo $hero_subtitle ?>
                </div>
                <a href="<?php echo $hero_button['url'] ?>"
                   class="btn btn-big btn-primary"><?php echo $hero_button['title'] ?></a>
                <img class="homepage-hero-img" style="object-fit: contain; object-position: bottom;"
                     src="<?= get_template_directory_uri(); ?>/assets/images/walk01c.png" alt=""/>
                <?php if (false): ?>
                    <div class="video_container homepage-hero-img">
                        <video id="introVideo1" muted="" playsinline="" data-video="1">
                            <source src="<?php echo $video_close_mp4['url'] ?>" type='video/mp4; codecs="hvc1"'>
                            <source src="<?php echo $video_close['url'] ?>" type='video/webm'>
                        </video>
                        <video id="introVideo2" muted="" playsinline="" data-video="2"
                               style="display:none;">
                            <source src="<?php echo $video_zoom_out_mp4['url'] ?>" type='video/mp4; codecs="hvc1"'>
                            <source src="<?php echo $video_zoom_out['url'] ?>" type='video/webm; codecs="vp9"'>
                        </video>
                        <video id="introVideo3" muted="" playsinline="" data-video="3"
                               preload="auto" style="display:none;">
                            <source src="<?php echo $video_walk_mp4['url'] ?>" type='video/mp4; codecs="hvc1"'>
                            <source src="<?php echo $video_walk['url'] ?>" type="video/webm">
                        </video>
                        <script type='text/javascript'>
                            jQuery(document).ready(function () {
                                let $ = jQuery;
                                if ($(window).width() > 768) {
                                    var player1 = document.getElementById('introVideo1');
                                    var player2 = document.getElementById('introVideo2');
                                    var player3 = document.getElementById('introVideo3');
                                    player2.load();
                                    player3.load();
                                    player2.play();
                                    player3.play();
                                    setTimeout(function () {
                                        player2.pause();
                                        player3.pause();
                                    }, 100);
                                    player1.addEventListener('ended', myHandler, false);
                                    player2.addEventListener('ended', myHandler, false);
                                    player3.addEventListener('ended', myHandler2, false);
                                    if ($('.preloader').css('display') != 'none') {
                                        setTimeout(function () {
                                            player1.play();
                                        }, 4000);
                                    } else {
                                        player1.play();
                                    }
                                } else {
                                    $('#introVideo2').hide();
                                    $('#introVideo3').hide();
                                    jQuery(window).ready(function () {
                                        setTimeout(function () {
                                            jQuery('#introVideo1')[0].play();
                                        }, 100);
                                    });
                                    var myVideo = document.getElementById('introVideo1');
                                    myVideo.addEventListener('timeupdate', function () {
                                        if (myVideo.currentTime >= myVideo.duration * .99) {
                                            // Reset the video to 0
                                            myVideo.currentTime = 0;
                                            // And play again
                                            myVideo.play();
                                        }
                                    });
                                }

                                function myHandler2(e) {
                                    player3.currentTime = 0;
                                    player3.play();
                                }

                                function myHandler(e) {
                                    var video_index = $(this).data('video');
                                    if (!e) {
                                        e = window.event;
                                    }
                                    video_index++;
                                    if (video_index > 3) return;
                                    $('#introVideo' + video_index)[0].play();
                                    $this = $(this);
                                    setTimeout(function () {
                                        $('#introVideo' + video_index).show();
                                        $this.hide();
                                        $this[0].pause();
                                    }, 1)
                                }
                            })
                        </script>
                        <script>
                            // jQuery(window).ready(function () {
                            //     setTimeout(function () {
                            //         jQuery('#introVideo')[0].play();
                            //     }, 100);
                            // });
                            // var myVideo = document.getElementById('introVideo');
                            // myVideo.addEventListener('timeupdate', function () {
                            //     if (myVideo.currentTime >= myVideo.duration * .9999999) {
                            //         // Reset the video to 0
                            //         myVideo.currentTime = 0;
                            //         // And play again
                            //         myVideo.play();
                            //     }
                            // });
                        </script>
                    </div>
                <?php endif; ?>
            </div>
            <div id="blur-circle-1"></div>
            <div id="blur-circle-2"></div>
        </div>

        <?php

        $vision_image = get_field('vision_image');
        $vision_content = get_field('vision_content');

        ?>

        <section class="homepage-vision" id="vision">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 homepage-vision-left">
                        <img src="<?php echo $vision_image['url'] ?>" alt=""/>
                    </div>
                    <div class="col-sm-6 homepage-vision-right">
                        <?php echo $vision_content['vision_title'] ?>
                        <div class="text-xl">
                            <?php echo $vision_content['vision_text'] ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="overflow_wrap" style="top: -35vh;">
                <div id="blur-circle-3"></div>
            </div>
            <div class="overflow_wrap" style="top: -40vh;">
                <div id="blur-circle-4"></div>
            </div>
        </section>

        <?php

        $benefits = get_field('benefits');

        ?>

        <div class="homepage-benefits" style="padding-bottom: 6rem;">
            <div class="container">
                <div class="row">
                    <?php
                    foreach ($benefits as $benefit) {
                        ?>
                        <div class="col-md-4">
                            <div class="homepage-benefits-item">
                                <video autoplay loop muted="" playsinline="" class="lazy">
                                    <source data-src="<?php echo $benefit['benefit_image']['mp4'] ?>"
                                            type='video/mp4; codecs="hvc1"'>
                                    <source data-src="<?php echo $benefit['benefit_image']['webm'] ?>"
                                            type="video/webm">
                                </video>
                                <div class="homepage-benefits-item-title">
                                    <?php echo $benefit['benefit_title'] ?>
                                </div>
                                <p class="text-s">
                                    <?php echo $benefit['benefit_text'] ?>
                                </p>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

        <!-- video section start -->
        <div class="video_section container" style="margin-bottom: 100px;">
            <video muted="" playsinline="" style="width: 100%; height: auto;" autoplay loop>
                <source src="<?= get_template_directory_uri(); ?>/video/FIN_min.mp4" type='video/mp4'>
            </video>
        </div>
        <!-- video section end -->

        <!-- COMPARISON SLIDER CODE START -->

        <?php
        $left_section = get_field('left_section');
        $right_section = get_field('right_section');
        ?>

        <?php if (false) : ?>
        <div class="comparison-slider-wrapper homepage-slider mobile_overflow">
            <!-- Comparison Slider - this div contain the slider with the individual images captions -->
            <div class="comparison-slider homepage-slider-images">
                <img src="<?php echo $right_section['image_right']['url'] ?>" alt="" style="max-width: none;"/>
                <!-- Div containing the image layed out on top from the left -->
                <div class="resize">
                    <img src="<?php echo $left_section['image_left']['url'] ?>" alt="" style="max-width: none;"/>
                </div>
                <!-- Divider where user will interact with the slider -->
                <div class="divider">
                    <div class="center_block">
                        <div class="left_block">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                 xmlns="http://www.w3.org/2000/svg"
                                 class="left_chevron">
                                <path d="M15.5 19L8.5 12L15.5 5" stroke="#0D214D" stroke-width="1.5"
                                      stroke-linecap="square"
                                      stroke-linejoin="round"/>
                            </svg>
                        </div>
                        <div class="right_block">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                 xmlns="http://www.w3.org/2000/svg"
                                 class="right_chevron">
                                <path d="M15.5 19L8.5 12L15.5 5" stroke="#0D214D" stroke-width="1.5"
                                      stroke-linecap="square"
                                      stroke-linejoin="round"/>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="homepage-slider-title left stroke-text normal rellax" data-rellax-speed="4">
                <div><?php echo $left_section['title_left'] ?></div>
            </div>
            <div class="homepage-slider-title right stroke-text normal rellax data-rellax-speed=" -4
            ">
            <div><?php echo $right_section['title_right'] ?></div>
        </div>
    </div>
<?php endif; ?>

    <!-- COMPARISON SLIDER CODE END -->
    <section class="homepage-outfit" id="outfits">
        <div class="container sticky_holder">

            <?php

            $args = array(
                'post_type' => 'outfit',
                'orderby' => 'date',
                'order' => 'asc'
            );
            // The Query
            $the_query = new WP_Query($args);

            $i = 1;
            // The Loop
            if ($the_query->have_posts()) {
                while ($the_query->have_posts()) {
                    $the_query->the_post();

                    if (get_field('show_hp') != true) continue;

                    if (get_field('outfit_number')) {
                        $outfit_number = get_field('outfit_number');
                    } else {
                        $outfit_number = 0 . $i;
                    }


                    $variants = get_field('colors');
                    $universe = get_field('universe_items');

                    // print_r($variants);

                    ?>
                    <div class="homepage-outfit-item homepage-outfit-item-odd <?php if ($i === 1) {
                        echo 'active_outfit';
                    } ?>" data-index="<?php echo $i ?>">
                        <div class="row align-items-center">
                            <div class="col-md-6 col-xl-7 homepage-outfit-item-left">
                                <div class="homepage-outfit-item-number d-lg-none"><span><?= $outfit_number; ?></span>
                                </div>
                                <div class="homepage-outfit-item-title outfit_title_mobile d-lg-none">outfit
                                    <br><span><?php the_title(); ?></span></div>
                                <div class="homepage-outfit-item-outline stroke-text normal">
                                    Outfit <?= $outfit_number; ?>
                                </div>
                                <div class="homepage-outfit-item-left-wrap">
                                    <div class="layer_loader"></div>
                                    <ul class="nav mb-4 custom_toggle" id="pills-tab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill"
                                                    data-bs-target="#pills-home-<?= $i; ?>" type="button" role="tab"
                                                    aria-controls="pills-home" aria-selected="true">Metaverse
                                            </button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill"
                                                    data-bs-target="#pills-profile-<?= $i; ?>" type="button" role="tab"
                                                    aria-controls="pills-profile" aria-selected="false">Universe
                                            </button>
                                        </li>
                                    </ul>

                                    <div class="tab-content h-100" id="pills-tabContent">
                                        <div class="tab-pane meta_tab fade show active h-100" id="pills-home-<?= $i; ?>"
                                             role="tabpanel"
                                             aria-labelledby="pills-home-tab">
                                            <?php
                                            $k = 1;
                                            foreach ($variants as $color) {
                                                // print_r($color['metaverse'][0]['photos_and_videos'])
                                                if ($color['metaverse'][0]['photos_and_videos']['type'] == 'image') {
                                                    continue;
                                                    ?>
                                                    <img data-color="<?php echo $color['color_code'] ?>"
                                                         src="<?php echo $color['metaverse'][0]['photos_and_videos']['url'] ?>"
                                                         class="h-100 outfit-img <?php if ($k === 1) {
                                                             echo 'active-img lazy';
                                                         } ?>" alt=""/>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <video data-color="<?php echo $color['color_code'] ?>"
                                                           class="outfit-img firstLoad <?php if ($k === 1) {
                                                               echo 'active-img lazy';
                                                           } ?>" muted="" playsinline="" loop
                                                           data-video="1" <?php if ($k === 1 && $i === 1) {
                                                        echo 'autoplay';
                                                    } ?>
                                                           data-poster="<?php echo $color['metaverse'][0]['poster'] ?>">
                                                        <source data-src="<?php echo $color['metaverse'][0]['video_mp4']['url'] ?>"
                                                                type='video/mp4; codecs=" hvc1"'>
                                                        <source data-src="<?php echo $color['metaverse'][0]['photos_and_videos']['url'] ?>"
                                                                type="video/webm">
                                                    </video>
                                                    <?php
                                                }
                                                ?>
                                                <button class="btn-share outfit_share" data-bs-toggle="modal"
                                                        data-bs-target="#shareModal"
                                                        data-url="<?= get_the_permalink(); ?>">Share <span
                                                            class="icon-share-outline"></span></button>
                                                <?php
                                                $k++;
                                            }

                                            ?>
                                            <div class="generating_label" style="display: none;">GENERATING
                                                <div class="dots"><span></span><span></span><span></span></div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade h-100" id="pills-profile-<?= $i; ?>" role="tabpanel"
                                             aria-labelledby="pills-profile-tab">
                                            <?php

                                            if ($universe[0]['photos_and_videos']['type'] == 'image') {
                                                ?>
                                                <img src="<?php echo $universe[0]['photos_and_videos']['url'] ?>"
                                                     class="h-100 outfit-img active-img" alt=""/>
                                                <?php
                                            } else {
                                                ?>
                                                <video class="outfit-img active-img lazy" autoplay="" muted=""
                                                       playsinline=""
                                                       data-video="1" loop>
                                                    <source data-src="<?php echo $universe[0]['video_mp4']['url'] ?>"
                                                            type='video/mp4; codecs=" hvc1"'>
                                                    <source data-src="<?php echo $universe[0]['photos_and_videos']['url'] ?>"
                                                            type="video/webm">
                                                </video>
                                                <?php
                                            }

                                            ?>
                                            <button class="btn-share outfit_share" data-bs-toggle="modal"
                                                    data-bs-target="#shareModal" data-url="<?= get_the_permalink(); ?>">
                                                Share <span class="icon-share-outline"></span></button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6 col-xl-5 homepage-outfit-item-right">
                                <div class="homepage-outfit-item-number d-none d-lg-block">
                                    <span><?= $outfit_number; ?></span></div>
                                <div class="homepage-outfit-item-title outfit_title_desktop d-none d-lg-block">outfit
                                    <br><span><?php the_title(); ?></span></div>
                                <div class="homepage-outfit-item-colors">
                                    <div class="text-m mb-2">Colors</div>
                                    <div class="form-check-group">
                                        <?php
                                        $j = 1;
                                        foreach ($variants as $color) {
                                            ?>

                                            <div class="form-check">
                                                <input class="form-check-input"
                                                       style="background-color: <?php echo $color['color_code'] ?>"
                                                       type="radio"
                                                       name="outfit_radios_<?php echo $i; ?>"
                                                       data-color="<?php echo $color['color_code'] ?>"
                                                       id="variant_color_<?php echo $j ?>" <?php if ($j === 1) {
                                                    echo "checked";
                                                } ?>>
                                            </div>

                                            <?php
                                            $j++;
                                        }
                                        ?>
                                    </div>
                                </div>
                                <a href="<?php echo the_permalink() ?>" class="btn btn-big btn-primary">Show more</a>
                            </div>
                        </div>
                    </div>

                    <?php
                    $i++;
                }
            } else {
                // no posts found
            }
            /* Restore original Post Data */
            wp_reset_postdata();
            ?>
        </div>
    </section>

<?php if ($show_more = get_field('show_more')) : ?>
    <div class="stroke-text normal show-more container mobile_overflow">
        <img class="me-3" src="<?php echo get_template_directory_uri() ?>/assets/images/big_down_arrow.svg" alt=""/>
        <a href="<?= $show_more['url']; ?>" class="hoverColor"><?= $show_more['title']; ?></a>
    </div>
<?php endif; ?>

<?php
$title_try_or_buy = get_field('title_try_or_buy');
$try_or_buy_items = get_field('try_or_buy_items');
$try_or_buy_button = get_field('try_or_buy_button');
?>


    <section class="homepage-tryorbuy" id="try-or-buy">

        <div class="overflow_wrap" style="top: 35vh;">
            <div id="blur-circle-5"></div>
        </div>
        <div class="overflow_wrap" style="top: 35vh;">
            <div id="blur-circle-6"></div>
        </div>
        <div class="container">
            <div class="homepage-tryorbuy-outline stroke-text normal rellax" data-rellax-speed="2" style="z-index: 1;">
                <div>try or buy</div>
            </div>
            <h2><?php echo $title_try_or_buy ?></h2>
            <div class="row">
                <?php
                $i = 0;
                foreach ($try_or_buy_items as $item) {
                    ?>
                    <div class="col-sm-4">
                        <div class="homepage-tryorbuy-item scrollAnim fadeInDown opacity-0"
                             data-delay="<?= 0.2 * $i; ?>">
                            <img src="<?php echo $item['item_image']['url'] ?>" alt=""/>
                            <div class="homepage-tryorbuy-item-title">
                                <?php echo $item['item_title'] ?>
                            </div>
                            <p class="text-s">
                                <?php echo $item['item_text'] ?>
                            </p>
                        </div>
                    </div>
                    <?php
                    $i++;
                }

                ?>
            </div>
            <div class="text-center">
                <?php // echo $try_or_buy_button['url'] ?>
                <a href="mailto:info@metarials.studio"
                   class="btn btn-big btn-primary"><?php echo $try_or_buy_button['title'] ?></a>
            </div>
        </div>
    </section>

<?php
$title_dressx = get_field('title_dressx');
$items_dressx = get_field('items_dressx');
$button_dressx = get_field('button_dressx');
?>

    <div class="homepage-dressx mobile_overflow">
        <div class="container">
            <div class="homepage-dressx-outline stroke-text normal rellax" data-rellax-speed="-2">
                <div>dressx</div>
            </div>
            <h2><?php echo $title_dressx ?></h2>
            <div class="row">
                <?php

                foreach ($items_dressx as $item_d) {
                    ?>
                    <div class="col-sm-4">
                        <div class="homepage-dressx-item">
                            <img src="<?php echo $item_d['dressx_image']['url'] ?>" alt=""/>
                            <div class="homepage-dressx-item-title">
                                <?php echo $item_d['dressx_title'] ?>
                            </div>
                            <div class="homepage-dressx-item-price">
                                <?php echo $item_d['dressx_price'] ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }

                ?>
            </div>
            <div class="text-center mt-3">
                <a href="<?php echo $button_dressx['url'] ?>"
                   class="btn btn-big btn-primary"
                   target="<?= $button_dressx['target'] == '_blank' ? '_blank' : '_self'; ?>"><?php echo $button_dressx['title'] ?></a>
            </div>
        </div>
    </div>


<?php
$filters_title = get_field('filters_title');
$filters_items = get_field('filters_items');
$count_items = count($filters_items);
$i = 1;
?>


    <div class="homepage-filters mobile_overflow">
        <div class="container">
            <div id="blur-circle-7"></div>
            <div class="homepage-filters-outline stroke-text normal rellax" data-rellax-speed="2">
                <div><?php echo $filters_title ?></div>
            </div>
            <div class="mockup"></div>
            <div class="owl-carousel owl-theme" id="filters-slider">
                <?php
                foreach ($filters_items as $filter) {
                    ?>
                    <div class="item">
                        <div class="row align-items-center">
                            <div class="homepage-filters-title d-md-none">
                                <?php echo $filter['filters_title'] ?>
                            </div>
                            <div class="col-sm-6 item-left">
                                <div class="homepage-filters-img">
                                    <video class="outfit-img active-img <?= $i == 1 ? 'lazy' : ''; ?>" autoplay=""
                                           muted="" playsinline=""
                                           data-video="1" loop
                                           data-poster="<?= $filter['filters_poster']; ?>">
                                        <source data-src="<?php echo $filter['filters_image']; ?>" type="video/mp4"
                                                codecs="hvc1">
                                    </video>
                                </div>
                            </div>
                            <div class="col-sm-6 item-right">
                                <div class="homepage-filters-title d-none d-md-block">
                                    <?php echo $filter['filters_title'] ?>
                                </div>
                                <div class="homepage-filters-count">
                                    0<?php echo $i ?><span>/0<?php echo $count_items; ?></span>
                                </div>
                                <div class="homepage-filters-name">
                                    <?php echo $filter['filters_name'] ?>
                                </div>
                                <a href="<?php echo $filter['filters_button']['url'] ?>"
                                   class="btn btn-big btn-primary"><?php echo $filter['filters_button']['title'] ?></a>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                }

                ?>
            </div>
        </div>
    </div>

<?php
$about_left = get_field('about_left');
$about_right = get_field('about_right');
?>

    <section class="homepage-about" id="about-me">
        <div class="overflow_wrap">
            <div id="blur-circle-8"></div>
        </div>
        <div class="container">
            <div class="homepage-about-outline stroke-text normal rellax" data-rellax-speed="2">
                <div>Creator</div>
            </div>
            <div class="row align-items-center">
                <div class="col-sm-6 homepage-about-left">
                    <h2><?php echo $about_left['about_title'] ?></h2>
                    <p class="text-m mb-4">
                        <?php echo $about_left['about_text'] ?>
                    </p>
                    <a href="mailto:info@metarials.studio"
                       class="btn btn-big btn-primary d-none d-md-inline-block"><?php echo $about_left['about_button']['title'] ?></a>
                </div>
                <div class="col-sm-5 ms-auto homepage-about-right">
                    <div class="homepage-about-right-wrap">
                        <img src="<?php echo $about_right['about_image']['url'] ?>" alt=""/>
                        <div class="homepage-about-right-bottom">
                            <h6><?php echo $about_right['about_name'] ?></h6>
                            <p class="text-xs">
                                <?php echo $about_right['about_position'] ?>
                            </p>
                        </div>
                    </div>
                </div>
                <a href="<?php echo $about_left['about_button']['url'] ?>"
                   class="btn btn-big btn-primary d-md-none mt-4"><?php echo $about_left['about_button']['title'] ?></a>
            </div>
        </div>
    </section>

<?php
$podcast_title = get_field('podcast_title');
$podcast_text = get_field('podcast_text');
$podcast_iframe = get_field('podcast_iframe');
?>

    <div class="homepage-podcast">
        <div class="overflow_wrap" style="top: -50vh;">
            <div id="blur-circle-9"></div>
        </div>
        <div class="container">
            <div class="row mb-md-5">
                <div class="col-sm-6">
                    <h2><?php echo $podcast_title ?></h2>
                </div>
                <div class="col-sm-5 ms-auto">
                    <p class="text-l">
                        <?php echo $podcast_text ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <iframe style="border-radius:12px"
                        src="https://open.spotify.com/embed/episode/6EqL1GjNg72oBuUnzjmuyJ?utm_source=generator&theme=0"
                        width="100%" height="352" frameBorder="0" allowfullscreen=""
                        allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"
                        loading="lazy"></iframe>
            </div>
        </div>
    </div>
    </div>

<?php
get_footer();
