<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Metarials
 */

?>
    <!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>METARIALS ◍ The next gen fashion</title>
        <link rel="preload" href="<?= get_template_directory_uri() ?>/assets/css/panchang.css" type="text/css"
              as="style" onload="this.onload=null;this.rel='stylesheet'">
        <link rel="preconnect" href="https://fonts.gstatic.com/">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri() ?>/favicon.ico"/>
        <link rel="apple-touch-icon" sizes="152x152"
              href="<?php echo get_template_directory_uri() ?>/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32"
              href="<?php echo get_template_directory_uri() ?>/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16"
              href="<?php echo get_template_directory_uri() ?>/favicon-16x16.png">
        <link rel="manifest" href="<?php echo get_template_directory_uri() ?>/site.webmanifest">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri() ?>/safari-pinned-tab.svg" color="#5bbad5">
        <link rel='preload' href="https://fonts.googleapis.com/css?family=Open+Sans" type='text/css' as="style"
              onload="this.rel='stylesheet'">
        <?php wp_head(); ?>
        <!-- Hotjar Tracking Code for my site -->
        <script>
            (function (h, o, t, j, a, r) {
                h.hj = h.hj || function () {
                    (h.hj.q = h.hj.q || []).push(arguments)
                };
                h._hjSettings = {hjid: 2889171, hjsv: 6};
                a = o.getElementsByTagName('head')[0];
                r = o.createElement('script');
                r.async = 1;
                r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
                a.appendChild(r);
            })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
        </script>
    </head>
    <script type="module" async="">
        const $ = jQuery;
        const isVisited = sessionStorage.getItem("visited");
        const shouldPreload = performance.navigation.type !== 1
        if ($('.preloader').length > 0 && (!isVisited || shouldPreload)) {
            $('.preloader').css({'display': 'flex'});
            setTimeout(function () {
                $('.preloader_text').show();
                $('.preloader_text').addClass('active');
            }, 10);
            sessionStorage.setItem('visited', 'yes');
        }
    </script>
    <script>
        // Set the options globally
        // to make LazyLoad self-initialize
        window.lazyLoadOptions = {
            // Your custom settings go here
        };
    </script>
    <style>
        .video_container video {
            width: 646px;
        }

        @media all and (min-width: 1600px) {
            .video_container video {
                max-height: 65vh;
                width: auto;
            }
        }

        .homepage-hero .homepage-hero-img {
            position: absolute;
            z-index: 1;
            right: 0;
            bottom: 0;
            display: flex;
            align-items: flex-end;
        }

        /*Styling preloader*/
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            min-height: 100vh;
            min-height: -webkit-fill-available;
            width: 100vw;
            z-index: 1000;
            background: white;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .preloader_text {
            color: transparent;
            font-family: "Panchang-Semibold";
            -webkit-text-stroke-color: #5c60d3;
            -webkit-text-fill-color: transparent;
            text-transform: uppercase;
            text-decoration: none;
            z-index: 2;
            font-size: 6rem;
            line-height: 140%;
            letter-spacing: 0.01em;
            background-size: 0% 100%;
            -webkit-text-stroke-width: 1.5px;
            background: linear-gradient(#5C60D3, #5C60D3) left no-repeat, transparent;
            background-size: 0% 100%;
            background-clip: text;
            -webkit-background-clip: text;
        }

        .blur_top {
            position: absolute;
            width: 560px;
            height: 560px;
            left: -112px;
            top: -135px;
            background: linear-gradient(94.06deg, rgba(252, 131, 205, 0.1) 5.89%, rgba(146, 82, 231, 0.1) 93.97%);
            filter: blur(100px);
            transform: translate3d(0, 0, 0);
            border-radius: 50%;
        }

        .blur_bottom {
            position: absolute;
            width: 726px;
            height: 726px;
            left: 1085px;
            top: 420px;
            background: linear-gradient(94.06deg, rgba(252, 131, 205, 0.15) 5.89%, rgba(146, 82, 231, 0.15) 93.97%);
            filter: blur(100px);
            transform: translate3d(0, 0, 0);
            border-radius: 50%;
        }

        @media all and (max-width: 991px) {
            .preloader_text {
                font-size: 2.5rem;
            }
        }

        .preloader_text.active {
            animation: preloader 5s cubic-bezier(0.165, 0.84, 0.43, 1);
            animation-fill-mode: forwards;
            background-position-x: 0;
            background-repeat: no-repeat;
        }

        @media all and (max-width: 768px) {
            .preloader_text {
                transform: rotate(90deg);
                font-size: 8vh;
            }
        }

        @keyframes preloader {
            0% {
                background-size: 0 100%;
                background-position-x: 0;
            }
            70% {
                background-size: 100% 100%;
                background-position-x: 0;
            }
            71% {
                background-position-x: 100%;
            }
            100% {
                background-position-x: 100%;
                background-size: 0 100%;
            }
        }
    </style>

<body>

<?php if (!is_single()) { ?>

    <header class="main-header">
        <div class="header-bottom">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-auto">
                        <a href="<?= home_url(); ?>" title="Metarials" class="header-logo">
                            <img class="header-logo header-logo-dark"
                                 src="<?php echo get_template_directory_uri() ?>/assets/images/logo.png" alt=""/>
                            <img class="header-logo header-logo-light"
                                 src="<?php echo get_template_directory_uri() ?>/assets/images/logo-dark.png" alt=""/>
                        </a>
                    </div>
                    <div class="col">
                        <nav class="main-menu">
                            <ul>
                                <li><a href="#vision">Vision</a></li>
                                <li><a href="#outfits">Outfits</a></li>
                                <li><a href="#try-or-buy">Buy</a></li>
                                <li><a href="#about-me">About</a></li>
                            </ul>
                            <div class="mobile-block">
                                <ul class="language-block">
                                    <?php if (ICL_LANGUAGE_CODE == 'en') : ?>
                                        <li><a class="active">EN</a></li>
                                        <li><a href="https://www.metarials.studio/sk">SK</a></li>
                                    <?php elseif (ICL_LANGUAGE_CODE == 'sk') : ?>
                                        <li><a href="<?= home_url(); ?>">EN</a></li>
                                        <li><a class="active">SK</a></li>
                                    <?php endif; ?>
                                </ul>
                                <a href="#contact" class="btn btn-small btn-primary w-100">Contact me</a>
                            </div>
                        </nav>
                        <div class="navTrigger ms-auto">
                            <i></i><i></i><i></i>
                        </div>
                    </div>
                    <div class="col-auto d-none d-lg-block ps-4 pe-4">
                        <a href="#contact" class="btn btn-small btn-secondary">Contact</a>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <?php
                        if (ICL_LANGUAGE_CODE == 'en') : ?>
                            <a href="<?= home_url(); ?>/sk">SK</a>
                        <?php elseif (ICL_LANGUAGE_CODE == 'sk') : ?>
                            <a href="https://www.metarials.studio/">EN</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </header>

<?php } ?>