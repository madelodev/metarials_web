'use strict';
var gulp = require('gulp');
var sass = require('gulp-dart-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
sass.compiler = require('sass');

gulp.task('sass', function () {
    return gulp.src('./scss/main.scss')
            .pipe(sass({outputStyle: 'compressed'}))
            .pipe(sourcemaps.init())
            .pipe(autoprefixer())
            .pipe(sourcemaps.write())
            .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest('./assets/css/'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./scss/*.scss', gulp.series('sass'));
    gulp.watch('./scss/general/*.scss', gulp.series('sass'));
    gulp.watch('./scss/utilities/*.scss', gulp.series('sass'));
    gulp.watch('./scss/components/*.scss', gulp.series('sass'));
    gulp.watch('./scss/sections/*.scss', gulp.series('sass'));
});

gulp.task('default', gulp.series('sass'));