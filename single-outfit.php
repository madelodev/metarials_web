<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Metarials
 */

get_header();


$id = get_the_ID();
$variants = get_field('colors', $id);
$universe = get_field('universe_items', $id);


?>

    <div class="detail-page">

        <div class="detail-left">
            <a href="/" class="back-link"><span class="icon-arrow_left"></span></a>
            <div class="detail-stroke-text stroke-text normal">
                metaverse
            </div>
        </div>
        <div class="detail-middle">
            <div class="row justify-content-center align-items-center detail-middle-top">
                <div class="col-auto d-md-block d-none">
                    <button type="button" class="btn-share" data-bs-toggle="modal" data-bs-target="#shareModal">
                        Share <span class="icon-share-outline"></span>
                    </button>
                </div>
                <div class="col-auto detail-middle-top-nav">
                    <ul class="nav" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="metaverse-tab" data-bs-toggle="pill"
                                    data-bs-target="#metaverse" type="button" role="tab" aria-controls="metaverse"
                                    aria-selected="true">Metaverse
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="universe-tab" data-bs-toggle="pill" data-bs-target="#universe"
                                    type="button" role="tab" aria-controls="universe" aria-selected="false">Universe
                            </button>
                        </li>
                    </ul>
                </div>
                <div class="col-auto d-md-block d-none">
                    <div class="btn-group">
                        <button type="button" class="dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                            <div class="form-check">
                                <input class="form-check-input"
                                       style="background-color: <?php echo $variants[0]['color_code'] ?>" type="radio"
                                       name="outfit_radios" id="outfit_radio_1">
                            </div>
                            <div class="form-arrow"></div>
                        </button>
                        <div class="dropdown-menu color_select">
                            <?php
                            $j = 1;
                            foreach ($variants as $color) {
                                ?>
                                <div class="form-check">
                                    <input class="form-check-input"
                                           style="background-color: <?php echo $color['color_code'] ?>" type="radio"
                                           name="outfit_radios_<?php echo $i; ?>"
                                           data-color="<?php echo $color['color_code'] ?>"
                                           id="variant_color_<?php echo $j ?>" <?php if ($j === 1) {
                                        echo "checked";
                                    } ?>>
                                </div>
                                <?php
                                $j++;
                            }
                            ?>
                        </div>
                        <div class="tooltip-custom">
                            Select metaverse to change color
                        </div>
                    </div>
<!--                    <div class="disabled-group" style="display:none">-->
<!--                        <span class="color" style="background-color: #794888"></span>-->
<!--                    </div>-->
                </div>
            </div>

            <div id="big" class="owl-carousel owl-theme">
                <?php

                foreach ($variants as $variant) {
                    ?>
                    <?php
                    foreach ($variant['metaverse'] as $item) {
                        ?>
                        <div class="item" data-color="<?php echo $variant['color_code'] ?>">
                            <?php if ($item['photos_and_videos']['type'] == 'image') { ?>
                                <img src="<?php echo $item['photos_and_videos']['url'] ?>" alt=""/>
                            <?php } else {
                                ?>
                                <video autoplay="" muted="" playsinline="" data-video="1" height="780" loop>
                                    <source data-src="<?php echo $item['video_mp4']['url'] ?>" type="video/mp4; codecs="
                                            hvc1
                                    "">
                                    <source data-src="<?php echo $item['photos_and_videos']['url'] ?>"
                                            type="video/webm">
                                </video>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                }
                foreach ($universe as $item) {
                    ?>
                    <div class="item" data-color="universe">
                        <?php if ($item['photos_and_videos']['type'] == 'image') { ?>
                            <img src="<?php echo $item['photos_and_videos']['url'] ?>" alt=""/>
                        <?php } else {
                            ?>
                            <video autoplay="" muted="" playsinline="" data-video="1" height="780" loop>
                                <source data-src="<?php echo $item['video_mp4']['url'] ?>" type="video/mp4; codecs="
                                        hvc1
                                "">
                                <source data-src="<?php echo $item['photos_and_videos']['url'] ?>" type="video/webm">
                            </video>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="detail-right">
            <div id="thumbs" class="owl-carousel owl-theme">
                <?php
                foreach ($variants as $variant) {
                    ?>
                    <?php
                    foreach ($variant['metaverse'] as $item) {
                        ?>
                        <div class="item" data-color="<?php echo $variant['color_code'] ?>">
                            <?php if ($item['photos_and_videos']['type'] == 'image') { ?>
                                <img src="<?php echo $item['photos_and_videos']['url'] ?>" alt=""/>
                            <?php } else {
                                ?>
                                <video autoplay="" muted="" playsinline="" data-video="1">
                                    <source data-src="<?php echo $item['photos_and_videos']['url'] ?>"
                                            type="video/webm">
                                    <source data-src="<?php echo $item['video_mp4']['url'] ?>" type="video/mp4">
                                </video>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                }
                foreach ($universe as $item) {
                    ?>
                    <div class="item" data-color="universe">
                        <?php if ($item['photos_and_videos']['type'] == 'image') { ?>
                            <img src="<?php echo $item['photos_and_videos']['url'] ?>" alt=""/>
                        <?php } else {
                            ?>
                            <video autoplay="" muted="" playsinline="" data-video="1" height="780" loop>
                                <source data-src="<?php echo $item['video_mp4']['url'] ?>" type="video/mp4; codecs="
                                        hvc1
                                "">
                                <source data-src="<?php echo $item['photos_and_videos']['url'] ?>" type="video/webm">
                            </video>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <div id="blur-circle-10"></div>
        <div class="product-name">OUTFIT <span><?php the_title(); ?></span></div>
        <div class="item-number"><?php the_title(); ?></div>

        <div class="detail-bottom">
            <div class="detail-bottom-btn">
                <span class="icon-more"></span>
                MORE
            </div>
            <div class="detail-bottom-content">
                <div class="detail-bottom-content-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <strong>Share</strong>
                        </div>
                        <div class="col-auto">
                            <ul class="detail-social">
                                <li>
                                    <a href="http://twitter.com/share?text=Metarials%20//%20The%20first%20digital%20fashion%20pop-up%20in%20Slovakia&url=https://metarials.studio/"
                                       target="_blank" data-twitter="http://twitter.com/share?text=Metarials%20//%20The%20first%20digital%20fashion%20pop-up%20in%20Slovakia&url="
                                       id="twitter_link"><span class="icon-twitter"></span></a></li>
                                <li><a href="https://discord.com/invite/FWrmp9W8MZ" target="_blank"><span class="icon-gamepad"></span></a></li>
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u=https://metarials.studio/" target="_blank"
                                       data-fb="https://www.facebook.com/sharer/sharer.php?u=" id="facebook_link"><span
                                                class="icon-facebook"></span></a></li>
                                <li><a onclick="copyData(selectLink, this)" style="cursor: pointer;"><span
                                                class="icon-link"></span></a><span
                                            class="tooltiptext">Link copied!</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="detail-bottom-content-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <strong>Colors</strong>
                        </div>
                        <div class="col-auto">
                            <div class="form-check-group color_select">
                                <?php
                                $j = 1;
                                foreach ($variants as $color) {
                                    ?>
                                    <div class="form-check not">
                                        <input class="form-check-input"
                                               style="background-color: <?php echo $color['color_code'] ?>" type="radio"
                                               name="outfit_radios_<?php echo $i; ?>"
                                               data-color="<?php echo $color['color_code'] ?>"
                                               id="variant_color_<?php echo $j ?>" <?php if ($j === 1) {
                                            echo "checked";
                                        } ?>>
                                    </div>
                                    <?php
                                    $j++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hidden_link" style="display: none !important;">
        <?php if (get_previous_post_link() != null) : ?>
            <div id="previous_link"><?php previous_post_link(); ?></div>
        <?php else: $args = array('post_type' => 'outfit', 'posts_per_page' => '1', 'orderby' => 'date', 'order' => 'desc');
            $recent_post = new WP_Query($args);
            if ($recent_post->have_posts()) :
                while ($recent_post->have_posts()) :
                    $recent_post->the_post(); ?>
                    <div id="previous_link"><a href="<?php echo get_the_permalink(get_the_ID()); ?>"></a></div>
                <?php endwhile; endif; endif;
        wp_reset_postdata(); ?>

        <?php if (get_next_post_link() != null) : ?>
            <div id="next_link"><?php next_post_link(); ?></div>
        <?php else: $args = array('post_type' => 'outfit', 'posts_per_page' => '1', 'orderby' => 'date', 'order' => 'asc');
            $recent_post = new WP_Query($args);
            if ($recent_post->have_posts()) :
                while ($recent_post->have_posts()) :
                    $recent_post->the_post(); ?>
                    <div id="next_link"><a href="<?php echo get_the_permalink(get_the_ID()); ?>"></a></div>
                <?php endwhile; endif; endif;
        wp_reset_postdata(); ?>

    </div>

<?php
get_footer();
