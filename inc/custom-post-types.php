<?php

/*
* Creating a function to create our CPT
*/
 
function outfit_cpt() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Outfits', 'Post Type General Name', 'metarials' ),
            'singular_name'       => _x( 'Outfit', 'Post Type Singular Name', 'metarials' ),
            'menu_name'           => __( 'Outfits', 'metarials' ),
            'parent_item_colon'   => __( 'Parent Outfit', 'metarials' ),
            'all_items'           => __( 'All Outfits', 'metarials' ),
            'view_item'           => __( 'View Outfit', 'metarials' ),
            'add_new_item'        => __( 'Add New Outfit', 'metarials' ),
            'add_new'             => __( 'Add New', 'metarials' ),
            'edit_item'           => __( 'Edit Outfit', 'metarials' ),
            'update_item'         => __( 'Update Outfit', 'metarials' ),
            'search_items'        => __( 'Search Outfit', 'metarials' ),
            'not_found'           => __( 'Not Found', 'metarials' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'metarials' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'Outfits', 'metarials' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'thumbnail' ),
            // You can associate this CPT with a taxonomy or custom taxonomy. 
            // 'taxonomies'          => array( 'genres' ),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */ 
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'show_in_rest' => true,
     
        );
         
        // Registering your Custom Post Type
        register_post_type( 'outfit', $args );
     
    }
     
    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not 
    * unnecessarily executed. 
    */
     
add_action( 'init', 'outfit_cpt', 0 );