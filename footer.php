<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Metarials
 */

?>

<!-- Modal -->
<div class="modal fade" id="shareModal" tabindex="-1" aria-labelledby="shareModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <h4>Share on<br /> social media</h4>
                <ul class="modal-social">
                    <li><a href="http://twitter.com/share?text=Metarials%20//%20The%20first%20digital%20fashion%20pop-up%20in%20Slovakia&url=https://metarials.studio/" target="_blank" data-twitter="http://twitter.com/share?text=Metarials%20//%20The%20first%20digital%20fashion%20pop-up%20in%20Slovakia&url=" id="twitter_link"><span class="icon-twitter"></span></a></li>
                    <li><a href="https://discord.com/invite/FWrmp9W8MZ" target="_blank"><span class="icon-gamepad"></span></a></li>
                    <li><a href="https://www.facebook.com/sharer/sharer.php?u=https://metarials.studio/" target="_blank" data-fb="https://www.facebook.com/sharer/sharer.php?u=" id="facebook_link"><span class="icon-facebook"></span></a></li>
                    <li><a onclick="copyData(selectLink, this)" style="cursor: pointer;"><span class="icon-link"></span></a><span class="tooltiptext">Link copied!</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="selectLink" style="position:absolute; z-index: -10; top: 0;"><?= "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?></div>
<script>
    function copyData(containerid, $this) {
        var range = document.createRange();
        range.selectNode(containerid); //changed here
        window.getSelection().removeAllRanges();
        window.getSelection().addRange(range);
        document.execCommand("copy");
        window.getSelection().removeAllRanges();
        console.log($this);
        var tooltip = $this.parentElement.querySelector('.tooltiptext');
        tooltip.style.visibility = 'visible';
        setTimeout(function (){tooltip.style.visibility = 'hidden';}, 700);
    }
</script>
<?php if (!is_single()) { ?>
    <section id="contact"></section>
    <footer>
        <div class="footer-links">
            <a href="https://www.instagram.com/metarials.studio/" target="_blank" class="stroke-text small hoverColor">Instagram</a>
            <a href="https://twitter.com/MetarialsStudio" target="_blank" class="stroke-text small hoverColor">twitter</a>
            <a href="https://discord.com/invite/FWrmp9W8MZ" target="_blank" class="stroke-text small hoverColor">Discord</a>
            <a href="https://www.facebook.com/MetarialsStudio" target="_blank" class="stroke-text small hoverColor">facebook</a>
        </div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-auto footer-col-1">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/H_Metarials logo horizontal.png" width="84" alt="" />
                </div>
                <div class="col text-center footer-col-2">
                    <a class="footer-mail" href="mailto:info@metarials.studio">info@metarials.studio</a>
                </div>
                <div class="col-auto footer-col-3">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/footer_u_logo.png" width="138" alt="" />
                </div>
            </div>
        </div>
    </footer>
<?php } ?>

<?php wp_footer(); ?>
<script
        async
        src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.6.1/dist/lazyload.min.js"
></script>

</body>
</html>
