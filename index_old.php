<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">    
        <title>Metarials</title>   
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="robots" content="noindex">
        <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css" />
        <link rel="preload" type="text/css" href="css/panchang.css"/>
        <link rel="stylesheet" type="text/css" href="css/style.css" /> 
        <link rel="stylesheet" type="text/css" href="dist/main.css" />
        <style>
            /*Styling preloader*/
            .preloader{
                /*
                Making the preloader floating over other elements.
                The preloader is visible by default.
                */
                position: fixed;
                top: 0;
                left: 0;
                width: 100vw;
                height: 100vh;
                z-index: 1000;
                background: white;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .preloader_text{
                color: transparent;
                font-family: "Panchang-Semibold";
                -webkit-text-stroke-color: #5c60d3;
                -webkit-text-fill-color: transparent;
                text-transform: uppercase;
                text-decoration: none;
                z-index: 2;
                font-size: 6rem;
                line-height: 140%;
                letter-spacing: 0.01em;
                background-size: 0% 100%;
                -webkit-text-stroke-width: 1.5px;
                background: linear-gradient(#5C60D3, #5C60D3) left no-repeat, transparent;
                background-size: 0% 100%;
                background-clip: text;
                -webkit-background-clip: text;
            }
            .preloader_text.active{
                background-size: 100% 100%;
                transition: all 3s ease-in;
            }
        </style>
    </head>
    <body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="module" async="">let $ = jQuery;
        var isVisited = sessionStorage.getItem("visited");
        if ($('.preloader').length > 0 ) {
            $('.preloader').css({'display': 'flex'});
           setTimeout(function (){$('.preloader_text').addClass('active');$('.preloader_text').show()}, 100);
        }</script>
        <?php include('templates/header.php') ?>
        <?php
        if (isset($_GET['page'])) {
            include('templates/' . $_GET['page'] . '.php');
        } else {
            include('templates/homepage.php');
        }
        ?>
        <?php include('templates/footer.php') ?>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/js/all.min.js" integrity="sha512-yFjZbTYRCJodnuyGlsKamNE/LlEaEAxSUDe5+u61mV8zzqJVFOH7TnULE2/PP/l5vKWpUNnF4VGVkXh3MjgLsg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
        <script src="js/custom.js"></script>
    </body>
</html>