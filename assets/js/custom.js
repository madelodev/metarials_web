jQuery(document).ready(function ($) {

    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 50) {
            jQuery('.main-header').addClass('sticky');
        } else {
            jQuery('.main-header').removeClass('sticky');
        }
    });

    jQuery("#filters-slider").owlCarousel({
        loop: true,
        nav: true,
        dots: false,
        margin: 0,
        items: 1,
        animateOut: 'fadeOut',
        lazyLoad: true,
        touchDrag: false,
        mouseDrag: false,
        responsive: {
            0: {
                dots: false
            },
            576: {
                dots: false
            }
        }
    }).on('changed.owl.carousel', function (event) {
        setTimeout(function () {
            $('#filters-slider').find('.active video').addClass('lazy');
            var myLazyLoad = new LazyLoad();
            myLazyLoad.update();
        }, 100);
    });

    /*var marginTopDynamic = function () {
     var navbarHeight = jQuery('.main-header').outerHeight();
     jQuery('.subpage').css('margin-top', navbarHeight);
     };
     
     jQuery(window).on('resize', function () {
     marginTopDynamic();
     });
     
     marginTopDynamic();*/

    jQuery('.navTrigger').click(function () {
        jQuery(this).toggleClass('active');
        jQuery(".main-header").toggleClass('open');
        jQuery(".mobile-header-label").toggle();
        jQuery(".header-logo-dark").toggle();
        jQuery(".header-logo-light").toggle();
        jQuery(".mobile-block").toggle();
    });

    if ($(window).width() < 991) {
        jQuery('.main-menu a').click(function () {
            jQuery('.navTrigger').click();
        });
    }

    jQuery('#contact-link').click(function (event) {
        event.preventDefault();
        jQuery('body,html').animate({
            scrollTop: jQuery('.contact-bottom-block').position().top - 50
        }, 500);
    });


    /* Product detail gallery */

    var bigimage = jQuery("#big");
    var thumbs = jQuery("#thumbs");
    //var totalslides = 10;
    var syncedSecondary = true;

    bigimage
        .owlCarousel({
            items: 1,
            nav: true,
            dots: false,
            smartSpeed: 200,
            slideSpeed: 500,
            margin: 0,
            animateOut: 'fadeOut',
            autoplay: false,
            loop: false,
            responsiveRefreshRate: 200,
            lazyLoad: true,
            mouseDrag: false
        }).on("changed.owl.carousel", syncPosition)
        .on("changed.owl.carousel", function () {
            setTimeout(function () {
                bigimage.find('.active video').addClass('lazy');
                thumbs.find('.current video').addClass('lazy');
                var myLazyLoad = new LazyLoad();
                myLazyLoad.update();
            }, 100);
        });

    setTimeout(function () {
        bigimage.find('.active video').addClass('lazy');
        thumbs.find('.current video').addClass('lazy');
        var myLazyLoad = new LazyLoad();
        myLazyLoad.update();
    }, 100);

    thumbs
        .on("initialized.owl.carousel", function () {
            thumbs
                .find(".owl-item")
                .eq(0)
                .addClass("current");
        })
        .owlCarousel({
            items: 6,
            nav: false,
            dots: false,
            smartSpeed: 200,
            slideSpeed: 500,
            responsiveRefreshRate: 200,
            autoplay: false,
            slideBy: 3,
            margin: 0,
            loop: false,
            lazyLoad: true,
            touchDrag: false,
            mouseDrag: false,

            responsive: {
                0: {
                    items: 5,
                    margin: 20
                },
                576: {
                    items: 5,
                    margin: 20
                },
                767.98: {}
            }
        })
        .on("changed.owl.carousel", syncPosition2);

    function syncPosition(el) {
        //if loop is set to false, then you have to uncomment the next line
        var current = el.item.index;

        //to disable loop, comment this block
        /* var count = el.item.count - 1;
         var current = Math.round(el.item.index - el.item.count / 2 - 0.5);
         
         if (current < 0) {
         current = count;
         }
         if (current > count) {
         current = 0;
         }*/
        //to this
        thumbs
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
        var onscreen = thumbs.find(".owl-item.active").length - 1;
        var start = thumbs
            .find(".owl-item.active")
            .first()
            .index();
        var end = thumbs
            .find(".owl-item.active")
            .last()
            .index();

        if (current > end) {
            thumbs.data("owl.carousel").to(current, 100, true);
        }
        if (current < start) {
            thumbs.data("owl.carousel").to(current - onscreen, 100, true);
        }
    }

    function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            bigimage.data("owl.carousel").to(number, 100, true);
        }
    }

    thumbs.on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = jQuery(this).index();
        bigimage.data("owl.carousel").to(number, 300, true);
    });

    /* END Product detail gallery */


    jQuery('.detail-bottom-btn').click(function () {
        jQuery(".detail-bottom").toggleClass('active');
    });

    /*
    * jquery comparision slider
    */
    $(document).ready(function () {

        // If the comparison slider is present on the page lets initialise it, this is good you will include this in the main js to prevent the code from running when not needed
        if ($(".comparison-slider")[0]) {
            let compSlider = $(".comparison-slider");

            //let's loop through the sliders and initialise each of them
            compSlider.each(function () {
                let compSliderWidth = $(this).width() + "px";
                $(this).find(".resize img").css({width: compSliderWidth});
                drags($(this).find(".divider"), $(this).find(".resize"), $(this));
            });

            //if the user resizes the windows lets update our variables and resize our images
            $(window).on("resize", function () {
                let compSliderWidth = compSlider.width() + "px";
                compSlider.find(".resize img").css({width: compSliderWidth});
            });
        }
    });

    // This is where all the magic happens
    // This is a modified version of the pen from Ege Görgülü - https://codepen.io/bamf/pen/jEpxOX - and you should check it out too.
    function drags(dragElement, resizeElement, container) {

        // This creates a variable that detects if the user is using touch input insted of the mouse.
        let touched = false;
        window.addEventListener('touchstart', function () {
            touched = true;
        });
        window.addEventListener('touchend', function () {
            touched = false;
        });

        // clicp the image and move the slider on interaction with the mouse or the touch input
        dragElement.on("mousedown touchstart", function (e) {

            //add classes to the emelents - good for css animations if you need it to
            dragElement.addClass("draggable");
            resizeElement.addClass("resizable");
            //create vars
            let startX = e.pageX ? e.pageX : e.originalEvent.touches[0].pageX;
            let dragWidth = dragElement.outerWidth();
            let posX = dragElement.offset().left + dragWidth - startX;
            let containerOffset = container.offset().left;
            let containerWidth = container.outerWidth();
            let minLeft = containerOffset + 10;
            let maxLeft = containerOffset + containerWidth - dragWidth - 10;

            //add event listner on the divider emelent
            dragElement.parents().on("mousemove touchmove", function (e) {

                // if the user is not using touch input let do preventDefault to prevent the user from slecting the images as he moves the silder arround.
                if (touched === false) {
                    e.preventDefault();
                }

                let moveX = e.pageX ? e.pageX : e.originalEvent.touches[0].pageX;
                let leftValue = moveX + posX - dragWidth;

                // stop the divider from going over the limits of the container
                if (leftValue < minLeft) {
                    leftValue = minLeft;
                } else if (leftValue > maxLeft) {
                    leftValue = maxLeft;
                }

                let widthValue = (leftValue + dragWidth / 2 - containerOffset) * 100 / containerWidth + "%";

                $(".draggable").css("left", widthValue).on("mouseup touchend touchcancel", function () {
                    $(this).removeClass("draggable");
                    resizeElement.removeClass("resizable");
                });

                $(".resizable").css("width", widthValue);

            }).on("mouseup touchend touchcancel", function () {
                dragElement.removeClass("draggable");
                resizeElement.removeClass("resizable");

            });

        }).on("mouseup touchend touchcancel", function (e) {
            // stop clicping the image and move the slider
            dragElement.removeClass("draggable");
            resizeElement.removeClass("resizable");

        });
    }

    /* navigation on scroll */
    var lastScrollTop = 0;
    var banner = $('.main-header');
    $(window).scroll(function () {
        var st = $(this).scrollTop();
        if (st > 10) {
            if (!banner.hasClass('sticky_nav'))
                banner.addClass('sticky_nav');
        }
        setTimeout(function () {
            if (st >= lastScrollTop && st > 100) {
                banner.addClass('hideNav');
            } else if (st < 10) {
                if (banner.hasClass('sticky_nav')) {
                    banner.removeClass('sticky_nav');
                }
                banner.removeClass('hideNav');
            } else if ($('.homepage-outfit').length > 0 && st >= $('.homepage-outfit').offset().top && st <= $('.homepage-outfit').offset().top + $('.homepage-outfit').height()) {
                banner.addClass('hideNav');
            } else {
                banner.removeClass('hideNav');
            }
            lastScrollTop = st;
        }, 1);
    });
    /* end */

    /* glider for toggle buttons */
// tab navigation active slider
    $('.custom_toggle').append("<div class='glider'></div>");
    let glider = $('.glider');
    glider.css('width', $(this).parent().find('li:first-child').width());
    $('.custom_toggle li').on('click', function () {
        glider = $(this).parent('.custom_toggle').find('.glider');
        $(this).parent().find('.active').removeClass('active');
        $(this).find('button').addClass('active');
        glider.css('width', $(this).width());
        let cat = $(this).data('id');
        var index = $(this).index();
        gap = 0;
        for (var i = 1; i < index + 1; i++) {
            gap += $(this).parent('.custom_toggle').find('li:nth-child(' + i + ')').width();
        }
        glider.css('transform', 'translateX(' + gap + 'px)');
    });

    $(window).load(function () {
        $('.custom_toggle .active').click();
    });


    //hp outfit changes
    if($('.homepage-outfit').length > 0){
        $(".form-check-input").on("click", function () {
            var input_color = $(this).data("color");
            $(".active_outfit .meta_tab .active-img")[0].pause();
            $(".active_outfit .meta_tab .outfit-img").removeClass("active-img");
            $('.outfit-img[data-color=' + input_color + ']').addClass("active-img").addClass('lazy');
            var myLazyLoad = new LazyLoad();
            // After your content has changed...
            myLazyLoad.update();
            playOutfit();
        });
    }

    $(".color_select .form-check-input").on("click", function () {
        var input_color = $(this).data("color");
        $('.dropdown-toggle .form-check-input').css('background-color', input_color);
        $('#thumbs .item').parent().show();
        $('#thumbs').find('.isOn').removeClass('isOn');
        $('#thumbs .item').not('[data-color="' + input_color + '"]').parent().hide();
        // $('#thumbs').find('[data-color="' + input_color + '"]').parent().click();
        $('#thumbs').find('[data-color="' + input_color + '"]').parent().addClass('isOn');
        $('#thumbs').find('.isOn').eq(0).click();
        $('#thumbs').find('[data-color="' + input_color + '"] video').addClass('lazy');
        var myLazyLoad = new LazyLoad();
        // After your content has changed...
        myLazyLoad.update();
    });
    $('#universe-tab').on("click", function () {
        var input_color = 'universe';
        $('#thumbs .item').parent().show();
        $('#thumbs').find('.isOn').removeClass('isOn');
        $('#thumbs .item').not('[data-color="' + input_color + '"]').parent().hide();
        // $('#thumbs').find('[data-color="' + input_color + '"]').parent().click();
        $('#thumbs').find('[data-color="' + input_color + '"]').parent().addClass('isOn1');
        $('#thumbs').find('.isOn1').click();
        $('#thumbs').find('[data-color="' + input_color + '"] video').addClass('lazy');
        var myLazyLoad = new LazyLoad();
        // After your content has changed...
        myLazyLoad.update();
    });
    $('#metaverse-tab').click(function () {
        $('.color_select .form-check:first-child .form-check-input').click();
    });
    if ($(window).width() > 991) {
        $(document).ready(function () {
            $('.dropdown-menu .form-check:first-child .form-check-input').click();
        })
    } else {
        $(document).ready(function () {
            $('.color_select .form-check:first-child .form-check-input').click();
        })
    }

    if ($('#previous_link a').length <= 0) {
        $('#big .owl-prev').hide();
    }
    if ($('#next_link a').length <= 0) {
        $('#big .owl-next').hide();
    }
    /* on click move between outfits */
    $('#big .owl-prev').unbind('click');
    $('#big .owl-next').unbind('click');
    $('#big .owl-prev').click(function (e) {
        e.preventDefault();
        window.location.href = $('#previous_link a').attr('href');
    });
    $('#big .owl-next').click(function (e) {
        e.preventDefault();
        window.location.href = $('#next_link a').attr('href');
    })

    $("#universe-tab").on("click", function () {
        $('.btn-group').addClass('disabled');
        $('.btn-group button').prop('disabled', true);
    });
    $("#metaverse-tab").on("click", function () {
        $('.btn-group').removeClass('disabled');
        $('.btn-group button').prop('disabled', false);
    });


    /* animation on scroll/load */
    var scrolls = $(".scrollAnim").map(function () {
        return this;
    }).get();
    var loads = $(".loadAnim").map(function () {
        return this;
    }).get();
    var wh = $(window).height();


    $(window).on('scroll', function () {
        for (var i = 0; i < scrolls.length; i++) {

            if ($(document).scrollTop() >= ($(scrolls[i]).offset().top) - (wh * 0.8)) {
                $(scrolls[i]).addClass('animate');
                let delay = $(scrolls[i]).data('delay');
                $(scrolls[i]).css('animationDelay', delay + "s");
            }
        }
    });

    $(document).ready(function () {
        for (var i = 0; i < loads.length; i++) {
            let delay = $(loads[i]).data('delay');
            $(loads[i]).addClass('animate');
            $(loads[i]).css('animationDelay', delay + "s");
        }
    });

    /* scramble animation on text */
    class TextScramble {
        constructor(el) {
            this.el = el
            this.chars = '!<>-_\\/[]{}—=+*^?#________'
            this.update = this.update.bind(this)
        }

        setText(newText) {
            const oldText = this.el.innerText
            const length = Math.max(oldText.length, newText.length)
            const promise = new Promise((resolve) => this.resolve = resolve)
            this.queue = []
            for (let i = 0; i < length; i++) {
                const from = oldText[i] || ''
                const to = newText[i] || ''
                const start = Math.floor(Math.random() * 40)
                const end = start + Math.floor(Math.random() * 40)
                this.queue.push({from, to, start, end})
            }
            cancelAnimationFrame(this.frameRequest)
            this.frame = 0
            this.update()
            return promise
        }

        update() {
            let output = ''
            let complete = 0
            for (let i = 0, n = this.queue.length; i < n; i++) {
                let {from, to, start, end, char} = this.queue[i]
                if (this.frame >= end) {
                    complete++
                    output += to
                } else if (this.frame >= start) {
                    if (!char || Math.random() < 0.28) {
                        char = this.randomChar()
                        this.queue[i].char = char
                    }
                    output += `<span class="dud">${char}</span>`
                } else {
                    output += from
                }
            }
            this.el.innerHTML = output
            if (complete === this.queue.length) {
                this.resolve()
            } else {
                this.frameRequest = requestAnimationFrame(this.update)
                this.frame++
            }
        }

        randomChar() {
            return this.chars[Math.floor(Math.random() * this.chars.length)]
        }
    }

    let counter = 0;
    const scrambleText = () => {
        const el = document.querySelector('.active_outfit .outfit_title_desktop span');
        const phrases = [];
        phrases.push($('.active_outfit .outfit_title_desktop span').text());
        const fx = new TextScramble(el);
        fx.setText(phrases[counter]);
    }
    scrambleTextMobile = () => {
        const el = document.querySelector('.active_outfit .outfit_title_mobile span');
        const phrases = [];
        phrases.push($('.active_outfit .outfit_title_mobile span').text());
        const fx = new TextScramble(el);
        fx.setText(phrases[counter]);
    }
    if ($('.detail-page').length > 0) {
        scrambleDetailNumber = () => {
            const el = document.querySelector('.detail-page .item-number');
            const phrases = [];
            phrases.push($('.detail-page .item-number').text());
            const fx = new TextScramble(el);
            fx.setText(phrases[counter]);
        }
        scrambleDetailName = () => {
            const el = document.querySelector('.detail-page .product-name span');
            const phrases = [];
            phrases.push($('.detail-page .product-name span').text());
            const fx = new TextScramble(el);
            fx.setText(phrases[counter]);
        }
        $(window).load(function () {
            scrambleDetailNumber();
            scrambleDetailName();
        })
    }

    /*
    * scroll section function
    */
    if ($('.homepage-outfit').length > 0) {
        $(document).ready(function () {
            var container = $('.homepage-outfit');
            var count = $('.homepage-outfit-item').length;
            var offset = 100;
            var height = $('.homepage-outfit-item').height();

            $('.sticky_holder').css('top', ($(window).height() - $('.sticky_holder').height()) / 2);
            container.css('height', ((height * 1.2) + (offset * 2)) * count + 'px');
            var activeItem = $('.active_outfit');
            $(window).scroll(function () {
                var scrollPos = $(window).scrollTop();
                var index = parseInt((activeItem.data('index')));
                offsetScroll = index * (height + offset);
                offsetItem = (index - 1) * (height + offset);
                if (scrollPos - container.offset().top > offsetScroll && index != count) {
                    activeItem.removeClass('active_outfit');
                    $('.homepage-outfit-item:nth-child(' + (index + 1) + ')').addClass('active_outfit');
                    activeItem = $('.active_outfit');
                    if ($(window).width() > 991) {
                        setTimeout(scrambleText, 100);
                    } else {
                        setTimeout(scrambleTextMobile, 100);
                    }
                    setTimeout(function (){playOutfit();}, 50);
                    activeItem.find('.custom_toggle .active').click();
                } else if (scrollPos - container.offset().top < offsetItem && index != 1) {
                    activeItem.removeClass('active_outfit');
                    $('.homepage-outfit-item:nth-child(' + (index - 1) + ')').addClass('active_outfit');
                    activeItem = $('.active_outfit');
                    if ($(window).width() > 991) {
                        setTimeout(scrambleText, 100);
                    } else {
                        setTimeout(scrambleTextMobile, 100);
                    }
                    setTimeout(function (){playOutfit();}, 50);
                    activeItem.find('.custom_toggle .active').click();
                }
                if ((scrollPos >= container.offset().top) && scrollPos <= container.offset().top + container.height()) {
                    currentScroll = scrollPos - container.offset().top - offsetItem;
                    activeItem.find('.homepage-outfit-item-outline').css('background-size', (currentScroll / height) * 100 + '% 100%');
                    activeItem.find('.layer_loader').css('height', (currentScroll / height) * 100 + '%');
                }
            });
        });
    }

    /* rellax js */
    $(document).ready(function () {
        var rellax = new Rellax('.rellax', {
            center: true
        });
    });


    /* header animation on scroll */
    var blocks = $("section").map(function () {
        return this;
    }).get();

    $(window).on('scroll', function () {
        for (var x = 0; x < blocks.length; x++) {
            var block = $(blocks[x]).offset().top;
            if ($(document).scrollTop() >= block - 250 && $(document).scrollTop() <= block + $(blocks[x]).height()) {
                $('header').find('.focused').removeClass('focused');
                var name = $(blocks[x]).attr('id');
                $('header').find('a[href="#' + name + '"]').addClass('focused');
            }
        }
    });

    /* scroll on click anchor */
    //scroll to section on anchor after click
    jQuery(document).on('click', 'a[href^="#"]', function (event) {
        if ($(this).attr('href') == '#') return;
        if (/^#/.test($(this).attr('href'))) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 500);
        } else {
            window.location.href = window.location.origin + $.attr(this, 'href');
        }
    });

    /* share outfits */
    $('.outfit_share').click(function (e) {
        var url = $(this).data('url');
        var url_twitter = $('#twitter_link').data('twitter');
        var url_fb = $('#facebook_link').data('fb');
        $('#twitter_link').attr('href', url_twitter + url);
        $('#facebook_link').attr('href', url_fb + url);
        $('#selectLink').text(url);
    })

    if ($('.sticky_holder').length > 0) {
        $(window).load(function (){
            var newLazyLoad = new LazyLoad({
                container: document.querySelector(".sticky_holder")
            });
        });
    }

    function playOutfit(){
        var video = $('.active_outfit .active-img');
        if(video.hasClass('firstLoad')){
            video.parent().find('.generating_label').fadeIn();
            video.addClass('generating');
            setTimeout(function (){
                video[0].play();
                video.removeClass('generating');
                video.parent().find('.generating_label').fadeOut();
            }, 1500);
            video.removeClass('firstLoad');
        }
        else{
            setTimeout(function (){
                video[0].play();
            }, 10);

        }
    }

    $(window).load(function (){
        if($('.active_outfit').length > 0){
            playOutfit();
        }
    });
});



